import { syncHistory } from 'react-router-redux'
import { createStore, applyMiddleware } from 'redux'
import { browserHistory } from 'react-router'
import { routeReducer } from 'react-router-redux'

const combineRouting = (reducer) => (state, action) => {
    state = reducer(state, action)
    state.routing = routeReducer(state.routing, action)
    return state
}

export default function configureStore(routes, reducer) {
    reducer = combineRouting(reducer)

    const reduxRouterMiddleware = syncHistory(browserHistory)
    const createStoreWithMiddleware = applyMiddleware(reduxRouterMiddleware)(createStore)

    const store = createStoreWithMiddleware(reducer)

    // Required for replaying actions from devtools to work
    reduxRouterMiddleware.listenForReplays(store)

    return store
}
