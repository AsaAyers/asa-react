import React from 'react'
import ReactDOM from 'react-dom'

import configureStore from './configure_store'
import { Provider } from 'react-redux'
import { Router, browserHistory } from 'react-router'


export default function boot(options) {
    const { domRoot, routes, reducer } = options



    const store = configureStore(routes, reducer)

    ReactDOM.render(
        React.createElement(Provider, { store },
            React.createElement(
                Router,
                { history: browserHistory },
                routes
            )

        ),
        domRoot
    )
}
