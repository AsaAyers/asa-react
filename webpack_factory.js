/* eslint no-var: [0] */
require('es6-promise').polyfill()
var path = require('path')
var webpack = require('webpack')
var autoprefixer = require('autoprefixer')


module.exports = function(options) {
    if (options == null) {
        options = {}
    }
    var env = options.env
    if (env == null) {
        env = {}
    }
    var projectRoot = options.projectRoot
    if (projectRoot == null) {
        throw new Error('projectRoot is required. use __dirname')
    }

    var entry = options.entry
    if (entry == null) {
        throw new Error('entry is required')
    }

    for (var key in env) {
        var value = (process.env[key] !== undefined ? process.env[key] : env[key])
        // Environment variables always come in as strings, so they may have to be
        // converted
        if (value === 'true') {
            value = true
        } else if (value === 'false') {
            value = false
        } else if (!isNaN(parseInt(value, 10))) {
            value = parseInt(value, 10)
        }

        env[key] = JSON.stringify(value)
    }

    var css = '[path][name]---[local]---[hash:base64:5]'
    var optionalPlugins = []
    var devtool = 'source-map'
    if (process.env.NODE_ENV === 'production') {
        css = '[hash:base64]'
        devtool = 'cheap-source-map'
        optionalPlugins.push(
            new webpack.optimize.UglifyJsPlugin({
                sourceMap: false,
                compress: { warnings: false }
            })
        )
    }


    return {
        context: path.join(projectRoot, 'src'),
        devtool: devtool,
        entry: entry,
        // https://github.com/webpack/webpack/issues/811#issuecomment-75451797
        resolve: {
            extensions: ['', '.js', '.jsx'],
            fallback: path.join(projectRoot, "node_modules")
        },
        resolveLoader: {
            fallback: path.join(projectRoot, "node_modules")
        },
        node: {
            fs: 'empty'
        },
        module: {
            loaders: [
                {
                    test: /\.css$/,
                    loader: [
                        'style',
                        'css?localIdentName=' + css,
                        'postcss',
                        'resolve-url'
                    ].join('!')
                },
                {
                    test   : /\.scss$/,
                    loader: [
                        'style',
                        'css?sourceMap&localIdentName=' + css,
                        'postcss',
                        'resolve-url',
                        'sass?sourceMap'
                    ].join('!')
                },
                {
                    test: /\.jsx?$/,
                    exclude: /node_modules/,
                    loader: 'babel',
                    query: {
                        cacheDirectory: true,
                    }
                }
            ]
        },
        plugins: [
            new webpack.DefinePlugin({
                'process.env': env
            }),
            new webpack.NoErrorsPlugin(),
        ].concat(optionalPlugins),
        output: {
            path: path.join(projectRoot, 'build'),
            filename: '[name].js',
            sourceMapFilename: "[hash].[file].map",
            chunkFilename: '[hash].[id].engage.js',
        },
        postcss: [
            autoprefixer({
                browsers: ['last 2 versions']
            })
        ],
        devServer: {
            inline: true,
        },
    }

}
