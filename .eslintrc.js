module.exports = {
    "rules": {
        "indent": [
            2,
            4
        ],
        "quotes": [ 0 ],
        "linebreak-style": [
            2,
            "unix"
        ],
        "semi": [
            2,
            "never"
        ],
        "comma-dangle": [ 0 ],
        "react/display-name": [1, { "acceptTranspilerName": true }],
        "react/jsx-boolean-value": 1,
        "react/jsx-no-undef": 1,
        "react/jsx-quotes": 1,
        "react/jsx-uses-react": 1,
        "react/jsx-uses-vars": 1,
        "react/no-did-mount-set-state": 1,
        "react/no-did-update-set-state": 1,
        "react/no-multi-comp": 1,
        "react/no-unknown-property": 1,
        "react/prop-types": 1,
        "react/react-in-jsx-scope": 1,
        "react/require-extension": 0,
        "react/self-closing-comp": 1,
        "react/sort-comp": [1, {
            "order": [
                "lifecycle",
                "everything-else",
                "/^render.+/",
                "render",
            ]
        }],
        "react/wrap-multilines": 1,
    },
    "env": {
        "es6": true,
        "node": true,
        "browser": true
    },
    "extends": "eslint:recommended",
    "ecmaFeatures": {
        "jsx": true,
        "experimentalObjectRestSpread": true,
        "modules": true,
    },
    "plugins": [
        "react"
    ]
}
